import './App.css';
import { Navbar,Container} from 'react-bootstrap';
import ToggleButtonExample from './pages/toggle';

function App() {
  return (
    <div className="App">
           <>
            <Navbar   variant="dark">
              <Container>
                <Navbar.Brand href="#home">
                  <div className="displaygrid">
                  <img
                    alt=""
                    src={"images/logo.png"}
                    width="200"
                    height="30"
                    className="d-inline-block align-top"
                  />{' '}
                    <span className="tagline">A market for conversation mentorship and performance</span>
                   
                    <span className="tagline searchbar">
                      <span className="searcTalent">Search New Talent  </span>
                    <input  className="searcTalenttext" type="text" placeholder="search"/></span>
                   
                    </div>
                   
                </Navbar.Brand>
                <label className='labe-sigin'>Sign up &nbsp;&nbsp; &nbsp;&nbsp;      Login</label>
                
                
              </Container>
            </Navbar>

        </>

       <div className="inner-contaier">
          <ToggleButtonExample/>
       </div>
     
        
      
    </div>
  );
}

export default App;
