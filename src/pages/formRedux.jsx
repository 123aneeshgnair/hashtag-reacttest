import React from 'react';
import { Formik, Field, Form } from 'formik';
import * as yup from 'yup';
import axios  from 'axios'; 
import { random } from 'lodash';
import {Alert} from 'react-bootstrap'; 



const reduxform = (props) => {
    // const validationSchema = yup.object({
    //     first_name: yup.string().required('Required'),
    //     last_name: yup.string().required('Required'),
    //     password: yup.string().required('Required'),
    //     email: yup.string().email('Invalid email format').required('Required'),
    //     username: yup.string().required('Required')
    // });
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
    const validationSchema = yup.object({
        first_name: yup.string().required('Required').min(3),
        last_name: yup.string().required('Required').min(3),
        password: yup.string().required('Required').min(8),
        email: yup.string().email('Invalid email format').required('Required'),
        username: yup.string().required('Required').min(3), 
        phone: yup.string().matches(phoneRegExp, 'Phone number is not valid')
    });

    let apiUrl;
    let account_name ;
    let showmsg=false;
    let response_message ='Signup success';
    if(props.account_type==1){
        apiUrl="https://admin.fanconvo.com/api/v3/sign-up/fan";
        account_name ="Fan";
        console.log("api is called for fan");
    }else{
        apiUrl="https://admin.fanconvo.com/api/v3/sign-up/talent";
        account_name ="Talent";
        console.log("api is called for talentt");
    }



    return (
        <>
            <h5>Create Your {account_name} Account</h5>
            <Formik
                initialValues={{
                    first_name: '',
                    last_name: '',
                    email: '',
                    password:'',
                    username:'',
                    phone:'53747368'+random(10,99),
                    timezone:"America/New_York",
                    captcha: true
                }}
                onSubmit={async (values) => {
                    
                    await new Promise((r) => setTimeout(r, 500));
                    axios.post( apiUrl, values)
                      .then(function (response) {
                        if(response.status == 200)
                        {
                            showmsg = true;
                            response_message= response.data.message;
                            console.log(response.data.message)                            
                        }
                      })
                      .catch(function (error) {
                      
                        alert("unable to create"); 

                      });






                }}

                validationSchema={validationSchema}
            >
                {({handleChange, handleBlur,touched,errors}) =>{

                    
                    const alertmsg=()=>
                    {
                        return(<Alert variant="success">
                        {response_message}
                </Alert>);
                    }

                return(   
                <Form>
                    <div className="container">

                        

                        {showmsg?alertmsg():""}

                        <div className="row inlinegrid">
                        <label htmlFor="first_name">First Name</label>
                        <Field id="first_name" name="first_name" placeholder="Jane" />
                        <label className="labelerror">{touched.first_name && errors.first_name}</label>
                        </div>
                        <div className="row inlinegrid">
                        <label htmlFor="last_name">Last Name</label>
                        <Field id="last_name" name="last_name" placeholder="Doe" />
                        <label className="labelerror">{touched.last_name && errors.last_name}</label>
                        </div>

                        <div className="row inlinegrid">
                        <label htmlFor="username">Username Name</label>
                        <Field id="username" name="username" placeholder="Usernaem" />
                        <label className="labelerror">{touched.username && errors.username}</label>
                        </div>


                        <div className="row inlinegrid">
                        <label htmlFor="email">Email</label>
                        <Field
                            id="email"
                            name="email"
                            placeholder="jane@acme.com"
                            type="email"
                        />
                         <label className="labelerror">{touched.email && errors.email}</label>
                        </div>

                        <div className="row inlinegrid">
                        <label htmlFor="email">Timezone</label>
                        <Field as="select" name="color">
                            <option value="America/New_York">America/New_York</option>
                        </Field>
                        </div>

                        <div className="row inlinegrid">
                        <label htmlFor="password">Password</label>
                        <Field id="password"  type="password" name="password" placeholder="Doe" />
                        <label className="labelerror">{touched.password && errors.password}</label>
                        </div>

                        <div className="agree-div">
                        <Field type="checkbox" name="checked" value="One" />
                        <span className="agreement">I agree to the <span className="termscondition">Terms and Condition</span></span>
                        </div>


                        <div className="row">
                            <button className="submitbtn" type="submit">Sign Up</button>
                        </div>
                    </div>
                </Form>
                );
             }}      
             </Formik>
        </>



    )

}

export default reduxform;


