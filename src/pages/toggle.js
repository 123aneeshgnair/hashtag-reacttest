import {React,useState} from 'react';
import {ButtonGroup,ToggleButton} from 'react-bootstrap'; 
import Form from './formRedux';


export default function ToggleButtonExample() {
    
    const [radioValue, setRadioValue] = useState('1');
  
    const radios = [
      { name: 'FAN SIGNUP', value: '1' },
      { name: 'TALENT SIGNUP', value: '2' }
     
    ];


 
   
    return (
      <>
        <ButtonGroup>
          {radios.map((radio, idx) => (
            <ToggleButton
              key={idx}
              id={`radio-${idx}`}
              type="radio"
              variant={idx % 2 ? 'outline-success succ ' : 'outline-success dang'}
              name="radio"
              value={radio.value}
              checked={radioValue === radio.value}
              onChange={(e) => setRadioValue(e.currentTarget.value)}
            >
              {radio.name}
            </ToggleButton>
          ))}
        </ButtonGroup>

       
        <Form account_type={radioValue}/>     


      </>
    );
  }
